# GASAG OM-T Coding Challenge - README #

Hallo und Herzlich Willkommen zu Deiner GASAG OM-T Coding Challenge. :)

Wir möchten Dir mit dieser Coding-Challenge die Möglichkeit geben deine bisherigen Kenntnisse in Sachen Web-Programmierung unter Beweis zu stellen. 
Wie Du die gestellten Aufgaben löst bleibt Dir größtenteils selbst überlassen, für uns zählt am Ende das Ergebnis und eine laufende Anwendung, die wir uns für die Bewertung genauer anschauen.

### Vorbereitung und Generelles ###

* Klone dieses Repository in deine lokale Entwicklungsumgebung und erstelle Dir einen Branch in dem du die weiter unten gestellten Aufgaben abarbeitest. Dein Branch sollte dabei folgender Namenskonvention folgen: YYYYMMDD_VORNAME_NACHNAME_coding_challenge
* Alle Änderungen an deiner App müssen über regelmäßige Git-Commits nachvollziehbar dokumentiert sein
* Um mit Git zu arbeiten, kannst du entweder die Konsole nutzen, oder dir einen sogenannten *Git-Client* herunterladen: https://www.sourcetreeapp.com/
* Weiterführende Links zu oben genannten Punkten: 
	* How to use Git in Bitbucket: https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud
* Für die Abarbeitung der Aufgaben hast du insgesamt **eine Woche** Zeit. Solltest du früher fertig werden, umso besser. Wie schnell du mit den Aufgaben fertig wirst, fließt jedoch nur bedingt in die Bewertung ein


### Die ToDo-List Webapp

![ToDo-List WebApp](https://bitbucket.org/gasagde/coding-challenge/raw/4c27181d7f533261f55c6d86c41ace0c8e17f7e9/to-do-list-apps.png)

Im Rahmen dieser Coding-Challenge darfst Du uns eine ToDo-List Webapp programmieren. 

Die App soll einem Nutzer die Möglihkeit geben, gewisse Daten aus einer bestimmten API abzufragen, und diese Daten dann in einer Liste abzuspeichern. 

Beispiele für Datenabfragen könnten sein:
1. Liste deiner Lieblingsfilme
2. Liste deiner Lieblingsspeisen
3. Liste deiner liebsten Reiseziele, oder Orte die du noch besuchen möchtest
4. Deine Lieblings-Videospiele

### Was sind deine Aufgaben? ###
Folgende Funktionen muss deine WebApp dabei erfüllen:
1. Hinzufügen und Löschen von Elementen in eine Liste
2. Die zu speichernden Elemente kommen aus einer API
3. Die gespeicherten Elemente werden über Besuche hinweg als Cookie im Browser gespeichert

Kostenlose APIs, die du zur Erarbeitung der Aufgaben nutzen kannst:
1. MealDB API: https://www.themealdb.com/api.php
2. IMBD API: https://imdb-api.com/api
3. Free to Play Games API: https://www.freetogame.com/api-doc
4. Country API: https://restcountries.eu/

Technische Rahmenbedingungen
Du darfst dir für die Entwicklung der App aussuchen, ob du ein modernes Frontend-Framework wie React, Vue.js oder Angular nutzen möchtest.
Solltest du andere vergleichbare Frameworks kennen, kannst du diese ebenfalls nutzen. Oder ganz sportlich, alles in plain Javascript schreiben. :)

Natürlich darfst du aber auch ein Backend Framework wie .NET Core MVC, oder Django (Python), nutzen.

### An wen melde ich mich wenn ich Fragen habe? ###

* slaabs@gasag.de oder chuemmer@gasag.de